using UnityEngine;

public class TriggerStart : MonoBehaviour
{
    private static readonly int Start = Animator.StringToHash("start");

    public Animator animator;

    private void OnTriggerEnter2D(Collider2D other)
    {
        animator.SetBool(Start, true);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        animator.SetBool(Start, false);
    }
}