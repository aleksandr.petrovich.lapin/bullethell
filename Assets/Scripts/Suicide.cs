using System.Collections;
using UnityEngine;

public class Suicide : MonoBehaviour
{
    public float timeDestroy;

    private void Start()
    {
        StartCoroutine(DestroyWithTime());
    }

    private IEnumerator DestroyWithTime()
    {
        yield return new WaitForSeconds(timeDestroy);
        Destroy(gameObject);
    }
}