using UnityEngine;

public class TriggerDamage : MonoBehaviour
{
    private void OnTriggerStay2D(Collider2D other)
    {
        var playerLife = other.GetComponent<PlayerLife>();
        if (playerLife != null)
        {
            playerLife.SetDamage(transform.position);
        }
    }
}