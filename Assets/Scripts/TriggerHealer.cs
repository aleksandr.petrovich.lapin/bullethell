using UnityEngine;

public class TriggerHealer : MonoBehaviour
{
    public GameObject destroyGameObject;

    private void Start()
    {
        if (destroyGameObject == null)
        {
            destroyGameObject = gameObject;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var playerLife = other.GetComponent<PlayerLife>();
        if (playerLife == null) return;
        playerLife.SetHeal();
        Destroy(destroyGameObject);
    }
}