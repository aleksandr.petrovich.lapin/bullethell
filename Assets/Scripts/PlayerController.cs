using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;

    private PlayerMove _playerMove;

    private void Start()
    {
        _playerMove = GetComponent<PlayerMove>();
    }

    private void Update()
    {
        var move = new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"), 0);
        var shiftValue = Mathf.Sqrt(move.x * move.x + move.y * move.y);
        if (shiftValue > 1)
            move /= shiftValue;
        _playerMove.SetMove(move * speed);
    }
}