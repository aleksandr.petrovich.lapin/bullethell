using System.Collections;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject prefabObject;
    public float timeSpawn;
    public float startTime;

    private void Start()
    {
        StartCoroutine(SpawnObject());
    }

    private IEnumerator SpawnObject()
    {
        yield return new WaitForSeconds(startTime);

        while (enabled)
        {
            var newObject = Instantiate(prefabObject, transform.position, Quaternion.identity);
            newObject.SetActive(true);
            yield return new WaitForSeconds(timeSpawn);
        }
    }
}