using UnityEngine;

public class StaticMove : MonoBehaviour
{
    public Vector3 move;

    private void FixedUpdate()
    {
        transform.position += move * Time.deltaTime;
    }
}