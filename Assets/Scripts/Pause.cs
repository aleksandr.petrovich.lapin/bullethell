using UnityEngine;

public class Pause : MonoBehaviour
{
    public GameObject pausePanel;
    public AudioSource audioSource;

    private void Update()
    {
        if (!Input.GetButtonDown("Pause")) return;
        if (pausePanel.activeSelf)
        {
            if (audioSource != null) audioSource.Play();
            pausePanel.SetActive(false);
            Time.timeScale = 1;
        }
        else
        {
            if (audioSource != null) audioSource.Pause();

            Time.timeScale = 0;
            pausePanel.SetActive(true);
        }
    }
}