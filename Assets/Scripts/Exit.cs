using UnityEngine;

public class Exit : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetButtonDown("Exit"))
        {
            Application.Quit();
        }
    }
}