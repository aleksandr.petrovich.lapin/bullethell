using UnityEngine;

public class MoveToObject : MonoBehaviour
{
    public Transform objectTransform;
    public float speed;

    private Vector3 _move;

    private void Update()
    {
        _move = objectTransform.position - transform.position;
        if (!Vector3.zero.Equals(_move))
        {
            _move *= speed / Mathf.Sqrt(_move.x * _move.x + _move.y * _move.y);
        }
    }

    private void FixedUpdate()
    {
        transform.position += _move * Time.deltaTime;
        _move = Vector3.zero;
    }
}