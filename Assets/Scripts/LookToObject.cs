using UnityEngine;

public class LookToObject : MonoBehaviour
{
    public Transform objectTransform;
    public Vector3 shift;

    private Vector3 _rotation;

    private void Start()
    {
        _rotation = transform.eulerAngles;
    }

    private void Update()
    {
        shift = objectTransform.position - transform.position;
        _rotation.z = Mathf.Atan2(-shift.x, shift.y) * 180 / Mathf.PI;
    }


    private void FixedUpdate()
    {
        transform.eulerAngles = _rotation;
    }
}