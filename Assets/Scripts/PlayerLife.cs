using System;
using UnityEngine;

public class PlayerLife : MonoBehaviour
{
    private static readonly int Life = Animator.StringToHash("life");
    private static readonly int Invulnerability = Animator.StringToHash("Invulnerability");

    public int life;
    public float timeInvulnerability;
    public float timeImmobility;
    public float speed;
    public GameObject hearts;
    public Animator scenarioAnimator;

    private bool _invulnerability;
    private Animator _animator;
    private PlayerMove _playerMove;
    private PlayerController _playerController;
    private Animator[] _animators;
    private Vector3 _shiftVector;
    private float _time;

    private bool SetLive(int newValue)
    {
        if (_animators == null || (newValue = Math.Max(0, Math.Min(_animators.Length, newValue))) == life) return false;
        life = newValue;
        scenarioAnimator.SetInteger(Life, life);
        for (var i = 0; i < life; ++i)
        {
            _animators[i].SetBool(Life, true);
        }

        for (var i = life; i < _animators.Length; ++i)
        {
            _animators[i].SetBool(Life, false);
        }

        return true;
    }

    private void Start()
    {
        _invulnerability = false;
        _animator = GetComponent<Animator>();
        _playerMove = GetComponent<PlayerMove>();
        _playerController = GetComponent<PlayerController>();
        _animators = hearts.GetComponentsInChildren<Animator>();
        _time = 0;
        
        var startLife = life;
        life = 0;
        SetLive(startLife);
    }

    private void Update()
    {
        if (!_invulnerability) return;
        _time += Time.deltaTime;
        if (!_playerController.enabled)
        {
            if (_time >= timeImmobility)
            {
                Debug.Log("снова можно двигаться");
                _playerController.enabled = true;
            }
            else
            {
                _playerMove.SetMove(_shiftVector * speed);
            }
        }

        if (_time >= timeInvulnerability)
        {
            Debug.Log("снова уезвим");
            _animator.SetBool(Invulnerability, _invulnerability = false);
            _time = 0;
        }
    }

    public void SetDamage(Vector3 positionTrigger)
    {
        if (_invulnerability || !SetLive(life - 1)) return;

        Debug.Log("Урон");
        _playerController.enabled = false;
        _animator.SetBool(Invulnerability, _invulnerability = true);
        _shiftVector = transform.position - positionTrigger;
        if (_shiftVector.Equals(Vector3.zero))
            _shiftVector = Vector3.right;
        else
            _shiftVector /= Mathf.Sqrt(_shiftVector.x * _shiftVector.x + _shiftVector.y * _shiftVector.y);
    }

    public void SetHeal()
    {
        SetLive(life + 1);
    }
}