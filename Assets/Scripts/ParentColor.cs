using UnityEngine;

public class ParentColor : MonoBehaviour
{
    public Color color;

    private SpriteRenderer[] _spriteRenderers;
    private Color _oldColor;

    private void SetColor()
    {
        if (color.Equals(_oldColor)) return;
        _oldColor = color;
        foreach (var spriteRenderer in _spriteRenderers)
        {
            spriteRenderer.color = color;
        }
    }

    private void Start()
    {
        _spriteRenderers = GetComponentsInChildren<SpriteRenderer>();
        SetColor();
    }

    private void Update()
    {
        SetColor();
    }
}