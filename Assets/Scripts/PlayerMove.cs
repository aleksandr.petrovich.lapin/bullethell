using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    private Rigidbody2D _rigidBody;
    private Vector3 _move;

    private void Start()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        _move = Vector3.zero;
    }

    private void FixedUpdate()
    {
        _rigidBody.MovePosition(transform.position + _move * Time.deltaTime);
        _move = Vector3.zero;
    }

    public void SetMove(Vector3 move)
    {
        _move = move;
    }
}