using UnityEngine;

public class SetStaticMove : MonoBehaviour
{
    public Transform objectTransform;
    public float speed;
    private void Start()
    {
        var staticMove = GetComponent<StaticMove>();
        if (staticMove == null) return;
        var move = objectTransform.position - transform.position;
        if (Vector3.zero.Equals(move)) move = Vector3.zero;
        else move /= Mathf.Sqrt(move.x * move.x + move.y * move.y);
        move *= speed;
        Debug.Log(move);
        staticMove.move = move;
    }
}